package ipythonfilecomparatorr;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JsonParser {

	public String getCode(String fileName) {
		// TODO Auto-generated method stub
		JSONParser parser = new JSONParser();
		
		String mainString = "";
		
		try{
//			String fileName = "C:\\Users\\tesla\\Desktop\\2011331051_E1.ipynb";
			Object mainObj = parser.parse(new FileReader(fileName));
			
			JSONObject cellObject = (JSONObject) mainObj;
			
			JSONArray cells = (JSONArray) cellObject.get("cells");
			
			char[][] C = new char[cells.size()][];
			int iCounter = 0;
			
			for(Object each : cells){
//				System.out.println(each);
				
				JSONObject eachCell = (JSONObject) each;
				
//				This code for Obtaining cell Type of a Json Object
				String cellType = (String) eachCell.get("cell_type").toString();
//				System.out.println(cellType);
				
				String flag = "code";
				if(cellType.equals(flag)){
					
//					This code for Obtaining Source Code of a Json Object
					String sourceCode = (String) eachCell.get("source").toString().replaceAll("[\";]", "");
					sourceCode = sourceCode.replaceAll("[' ']+", " ");
					sourceCode = sourceCode.replaceAll("^\\s|\n\\s|\\s$", "");
					sourceCode = sourceCode.replaceAll("\\r\\n|\\r|\\n", "");
					sourceCode = sourceCode.replace("\\", "");
//					System.out.println(sourceCode);
					char[] finalSource = new char[sourceCode.length()-2];
					if(sourceCode.length()>2){
						for(int i = 0; i < sourceCode.length()-2; i++){
							finalSource[i] = sourceCode.charAt(i+1);
						}
					}
//					System.out.println(finalSource);
					String temp = String.valueOf(finalSource);
					mainString += temp + " ";
//					System.out.println("-----------------------------------");
				}
				
				//
				
			}
		
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}catch(ParseException e){
			e.printStackTrace();
		}catch(NullPointerException e){
			e.printStackTrace();
		}
		
//		System.out.println(mainString);
		return mainString;
	}

}
