/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipythonfilecomparatorr;

import static ipythonfilecomparatorr.FXMLDocumentController.files;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tesla
 */
public class Service {

    public List<Each> getComparison() {

        JsonParser jp = new JsonParser();
        CosineSimilarity cs = new CosineSimilarity();

//		Each each = new Each();
        List<Each> eachList = new ArrayList<Each>();
        for (String firstFile : files) {
            String f1 = firstFile;
            IPythonFile ipy1 = new IPythonFile(f1);
            for (String secondFile : files) {
                if (firstFile.equals(secondFile)) {
                    continue;
                }
                String f2 = secondFile;

                Each each = new Each();

                IPythonFile ipy2 = new IPythonFile(f2);

//				Set the Each properties
                each.setFileName1(ipy1.getName());
                each.setFileName2(ipy2.getName());
                each.setPath1(f1);
                each.setPath2(f2);

                double percentage = (cs.CosineSimilarityScore(jp.getCode(f1), jp.getCode(f2))) * 100;
                each.setPercentage(percentage);

                eachList.add(each);
                System.out.println(each);
            }
        }
        
        return eachList;

    }

}
