package ipythonfilecomparatorr;

public class IPythonFile {
	
	String fileName = null;
	
	public IPythonFile(String name){
		this.fileName = name;
	}
	
	public String getName(){
		int len = fileName.length();
		String name = "";
		char[] tempName = fileName.toCharArray();
		for(int i = len-7; i >= 0; i--){
			char c = fileName.charAt(i);
			if(c == '\\') break;
			else
				name += String.valueOf(c);
		}		
		StringBuffer buffer = new StringBuffer(name);
		return buffer.reverse().toString();
	}
		
}
