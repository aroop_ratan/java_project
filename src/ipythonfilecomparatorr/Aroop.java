/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipythonfilecomparatorr;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author sunny
 */
public class Aroop extends Application {

    @Override
    public void start(Stage stage) throws Exception {

        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));

        GridPane gridPane = new GridPane();

        List<Comparison> comp = new ArrayList<>();

        Comparison c1 = new Comparison();
        c1.setRoll1("2011331033");
        c1.setPercentage(40);
        c1.setRoll2("2011331034");

        Comparison c2 = new Comparison();
        c2.setRoll1("2011331033");
        c2.setPercentage(60);
        c2.setRoll2("2011331035");

        Comparison c3 = new Comparison();
        c3.setRoll1("2011331034");
        c3.setPercentage(80);
        c3.setRoll2("2011331035");

        comp.add(c1);
        comp.add(c2);
        comp.add(c3);

        gridPane.setGridLinesVisible(true);

        int row = 0;
        int col = 1;
        List<String> list = new ArrayList<>();

        list.add("2011331033");
        list.add("2011331034");
        list.add("2011331035");

        for (String s : list) {

            Label horizontalLabel = new Label(s);
            horizontalLabel.setPadding(new Insets(5,5,5,5));
            gridPane.add(horizontalLabel, col++, row);

        }

        row = 1;

        for (String s1 : list) {

            Label verticalLabel = new Label(s1);
            verticalLabel.setPadding(new Insets(5,5,5,5));
            gridPane.add(verticalLabel, 0, row);

            col = 1;
            for (String s2 : list) {

             //   gridPane.add(new Label(s1), col++, row);
                boolean flag = false;
                for (Comparison c : comp) {

                    if (c.getRoll2().equalsIgnoreCase(s2) && c.getRoll1().equalsIgnoreCase(s1)) {

                        String s = "";
                        s += c.getRoll1() + " " + c.getRoll2();
                        
                        double val = c.getPercentage();
                        
                        Button button = new Button(String.format("%.2f", val));
                        button.setId(s);
                        button.setPadding(new Insets(5,5,5,5));
                        
                        if(val<=40){
                            button.setStyle("-fx-background-color:green;");
                        }
                        else if(val>40 && val<=60){
                            button.setStyle("-fx-background-color:yellow;");
                        }
                        else if(val>=80){
                            button.setStyle("-fx-background-color:red;");
                        }

                        button.setOnAction(e -> {
                            System.err.println(button.getId());
                            
                        });

                        gridPane.add(button, col++, row);
                        flag = true;
                    }
                }
                if (!flag) {
                    Label emptyLabel = new Label(" ");
                    emptyLabel.setPadding(new Insets(5,5,5,5));
                    gridPane.add(emptyLabel, col++, row);
                }

            }

            row++;

        }

        Scene scene = new Scene(gridPane, 400, 300);
        stage.setScene(scene);

        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
