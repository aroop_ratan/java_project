/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipythonfilecomparatorr;

/**
 *
 * @author sunny
 */
public class Comparison {

    private String roll1;
    private String roll2;
    private String file1;
    private String file2;
    private double percentage;

    public String getRoll1() {
        return roll1;
    }

    public void setRoll1(String roll1) {
        this.roll1 = roll1;
    }

    public String getRoll2() {
        return roll2;
    }

    public void setRoll2(String roll2) {
        this.roll2 = roll2;
    }

    public String getFile1() {
        return file1;
    }

    public void setFile1(String file1) {
        this.file1 = file1;
    }

   
    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public String getFile2() {
        return file2;
    }

    public void setFile2(String file2) {
        this.file2 = file2;
    }

    @Override
    public String toString() {
        return "Comparison{" + "roll1=" + roll1 + ", roll2=" + roll2 + ", file1=" + file1 + ", file2=" + file2 + ", percentage=" + percentage + '}';
    }

   
    
    
    

}
