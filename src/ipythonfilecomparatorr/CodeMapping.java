package ipythonfilecomparatorr;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class CodeMapping {

	public HashMap<String, String> getCodeMapping(String fileName){
//	public static void main(String[] arg){
		
		JSONParser parser = new JSONParser();
		HashMap<String, String> mapping = new HashMap<String, String>();
		try{
//			String fileName = "C:\\Users\\tesla\\Desktop\\2011331051_E1.ipynb";
			Object mainObj = parser.parse(new FileReader(fileName));
			JSONObject cellObject = (JSONObject) mainObj;
			JSONArray cells = (JSONArray) cellObject.get("cells");
			
//			Starting mapping here
			String markDown = null;
			String code = null;
			for(Object each : cells){
				
				JSONObject eachCell = (JSONObject) each;
				
				String cellType = (String) eachCell.get("cell_type").toString();
				if(cellType.equals("markdown")){
					
//					This code for Obtaining Source Code of a Json Object
					String sourceCode = (String) eachCell.get("source").toString().replaceAll("\\<.*?\\>", "");
					sourceCode = sourceCode.replaceAll("[' ']+", " ").replaceAll("[\";]", "");;
					sourceCode = sourceCode.replaceAll("^\\s|\n\\s|\\s$", "");
					sourceCode = sourceCode.replaceAll("\\r\\n|\\r|\\n", "");
					sourceCode = sourceCode.replace("\\", "");
//					System.out.println(sourceCode);
					char[] finalSource = new char[sourceCode.length()-2];
					if(sourceCode.length()>2){
						for(int i = 0; i < sourceCode.length()-2; i++){
							finalSource[i] = sourceCode.charAt(i+1);
						}
					}
//					System.out.println(finalSource);
					markDown = String.valueOf(finalSource);

//					System.out.println("-----------------------------------");
				}
				else if(cellType.equals("code")){
					
//					This code for Obtaining Source Code of a Json Object
					String sourceCode = (String) eachCell.get("source").toString().replaceAll("[\";]", "");
					sourceCode = sourceCode.replaceAll("[' ']+", " ");
					sourceCode = sourceCode.replaceAll("^\\s|\n\\s|\\s$", "");
					sourceCode = sourceCode.replaceAll("\\r\\n|\\r|\\n", "");
					sourceCode = sourceCode.replace("\\", "");
//					System.out.println(sourceCode);
					char[] finalSource = new char[sourceCode.length()-2];
					if(sourceCode.length()>2){
						for(int i = 0; i < sourceCode.length()-2; i++){
							finalSource[i] = sourceCode.charAt(i+1);
						}
					}
//					System.out.println(finalSource);
					code = String.valueOf(finalSource);
					
					mapping.put(markDown, code);

//					System.out.println("-----------------------------------");
				}
			}
		
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}catch(ParseException e){
			e.printStackTrace();
		}catch(NullPointerException e){
			e.printStackTrace();
		}
		
//		System.out.println(mapping);
		return mapping;
	}
	
	public HashMap<String, Double> getOneByOneResult(String s1, String s2){
		
		CosineSimilarity cs = new CosineSimilarity();
		HashMap<String, String> first = getCodeMapping(s1);
		HashMap<String, String> second = getCodeMapping(s2);
		
		HashMap<String, Double> result = new HashMap<String, Double>();
		
		for(String e : first.keySet()){
			if(second.containsKey(e)){
				Double r = cs.CosineSimilarityScore(first.get(e), second.get(e));
				result.put(e, r);
			}
		}
		return result;
	}
}
